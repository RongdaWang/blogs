async function like (event, id) {
	let response = await fetch('/posts/like/' + id)
	if (response.ok) {
		let data = (await response.json())
		showMessage(data.message,'success')
		document.getElementById('liked').textContent = parseInt(document.getElementById('liked').textContent) + 1
	} else {
		let data = (await response.json())
		showMessage(data.message)
	}
}


async function tag (event, id) {
	let response = await fetch('/posts/tag/' + id)
	if (response.ok) {
		document.getElementById('tag').style.display = 'none'
		document.getElementById('untag').style.display = 'inline'
		let data = (await response.json())
		showMessage(data.message,'success')
	} else {
		let data = (await response.json())
		showMessage(data.message)
	}
}


async function untag (event, id) {
	let response = await fetch('/posts/untag/' + id)
	if (response.ok) {
		document.getElementById('tag').style.display = 'inline'
		document.getElementById('untag').style.display = 'none'
		let data = (await response.json())
		showMessage(data.message,'success')
	} else {
		let data = (await response.json())
		showMessage(data.message)
	}
}

function showMessage (message, type = 'danger') {
	document.getElementById('message').style.display = 'block';
	document.getElementById('message').innerHTML = `<div class="alert alert-${type} message" role="alert">
		${message}
	</div>`

	setTimeout(e => {
		document.getElementById('message').style.display = 'none'
	}, 5000)
}

async function removeItem (event, id, title) {
	event.preventDefault();
	let ok = confirm(`Untag post ${title} ?`)
	if (!ok)
		return;
	let response = await fetch('/posts/untag/' + id)
	if (response.ok) {
		let node = document.getElementById(`post-${id}`)
		node.parentNode.removeChild(node);
	} else {
		let data = (await response.json())
		alert(data.message)
	}
}

// boostrap for show tootips
// https://getbootstrap.com/docs/4.0/components/tooltips/
$(function() {
	$('[data-toggle="tooltip"]').tooltip()
	$('input[rel="txtTooltip"]').tooltip();
})





