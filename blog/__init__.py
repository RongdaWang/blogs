from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
app = Flask(__name__)
app.config['SECRET_KEY'] = 'abcd'

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://c2063316:Wangji19970904@csmysql.cs.cf.ac.uk:3306/c2063316_db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)

from blog import routes
