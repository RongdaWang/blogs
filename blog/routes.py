import os
from flask import render_template, url_for, request, redirect, flash, session, jsonify, Response
from blog import app, db
from datetime import date, timedelta, datetime
from blog.models import Post, User, Comment, Rating, Tag, Extra
from blog.forms import RegistrationForm, LoginForm, CommentForm, SearchForm
from flask_login import login_user, current_user, logout_user, login_required
from sqlalchemy import or_, desc, asc, exc, exists, func


@app.route("/")
@app.route("/home", methods=['GET', 'POST'])
def home():
    form = SearchForm()
    posts = Post.query.filter(func.DATE(Post.created_at) >= (
        datetime.now()-timedelta(days=30))).order_by(desc(Post.created_at)).limit(6).all()
    hotTopics = Post.query.filter(Post.hot == True).limit(3).all()
    return render_template('home.html', posts=posts, topics=hotTopics, title='home', form=form)


@app.route("/posts/<int:post_id>", methods=['GET', 'POST'])
def post(post_id):
    form = CommentForm()
    print(form.validate_on_submit())
    if form.validate_on_submit():
        if current_user.is_authenticated:
            comment = Comment(user_id=current_user.id, post_id=post_id,
                              content=form.content.data)
            db.session.add(comment)
            db.session.commit()
        else:
            return redirect('/login')

    post = Post.query.get_or_404(post_id)
    comments = Comment.query.filter_by(post_id=post_id).join(
        User, User.id == Comment.user_id).add_columns(User.id, User.username).order_by(desc(Comment.created_at)).all()
    liked = Rating.query.filter_by(post_id=post_id).count()
    tagged = []
    if current_user.is_authenticated:
        tagged = db.session.query(Tag.query.filter_by(
            user_id=current_user.id, post_id=post_id).exists()).scalar()
    form.content.data = ""
    return render_template('post.html', post=post, comments=comments, liked=liked, tagged=tagged, form=form)


@app.route("/posts", methods=['GET'])
def posts():
    order = request.args.get('order')
    orderby = asc(Post.created_at) if order == 'asc' else desc(Post.created_at)
    posts = Post.query.order_by(orderby).all()
    return render_template('posts.html', posts=posts, title="All Posts")


@app.route("/posts/tagged", methods=['GET'])
def tagged():
    if current_user.is_authenticated:
        posts = Tag.query.filter_by(user_id=current_user.id).join(Post, Post.id == Tag.post_id).add_columns(
            Post.id, Post.title, Post.content, Post.created_at, Post.image_url).order_by(desc(Post.created_at)).all()
        print(posts)
        return render_template('posts.html', posts=posts, title="Tagged Posts")
    else:
        return redirect('/login')


@app.route("/posts/tag/<int:post_id>", methods=['GET'])
def tag(post_id):
    if current_user.is_authenticated:
        try:
            tag = Tag(user_id=current_user.id, post_id=post_id)
            db.session.add(tag)
            db.session.commit()
        except exc.SQLAlchemyError:
            return jsonify({"message": "You have tagged this post"}), 400
        return jsonify({"message": "Tagged successfully"}), 200
    else:
        return jsonify({"message": "Please log in"}), 401


@app.route("/posts/untag/<int:post_id>", methods=['GET'])
def untag(post_id):
    if current_user.is_authenticated:
        try:
            tag = Tag.query.filter_by(
                user_id=current_user.id, post_id=post_id).one()
            db.session.delete(tag)
            db.session.commit()
        except exc.SQLAlchemyError:
            return jsonify({"message": "You have't tagged this post"}), 400
        return jsonify({"message": "Untagged successfully"}), 200
    else:
        return jsonify({"message": "Please log in"}), 401


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data,
                    email=form.email.data, password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created.  You can now log in.')
        return redirect(url_for('login'))

    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()

        if user is not None and user.verify_password(form.password.data):
            login_user(user)
            flash('You are now logged in.')
            return redirect(url_for('home'))
        flash('Invalid username or password.')

        return render_template('login.html', form=form)

    return render_template('login.html', title='Login', form=form)


@app.route("/posts/like/<int:post_id>")
def like(post_id):
    if current_user.is_authenticated:
        like = Rating(user_id=current_user.id, post_id=post_id)
        db.session.add(like)
        try:
            db.session.commit()
        except exc.SQLAlchemyError:
            return jsonify({"message": "You have liked the post"}), 400
        return jsonify({"message": "liked successfully"}), 200
    else:
        return jsonify({"message": "Please log in"}), 401


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route("/disclaimer")
def disclaimer():
    data = Extra.query.filter_by(title='Disclaimer').first()
    return render_template('extra.html', data=data)


@app.route("/policy")
def policy():
    data = Extra.query.filter_by(title='Privacy Policy').first()
    return render_template('extra.html', data=data)



@app.route("/search", methods=['POST'])
def search():
    form = SearchForm()

    posts = Post.query.filter(or_(Post.title.like('%' + form.keyword.data + '%'),
                                  Post.content.like('%' + form.keyword.data + '%'))).order_by(Post.created_at).all()
    return render_template('search.html', posts=posts, form=form, title="Search results")
